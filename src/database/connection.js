import sql from "mssql";
import config from "../config";

const configDatabase = {
  user: config.dbUser,
  password: config.dbPassword,
  server: config.dbServer,
  database: config.dbDatabase,
  options: {
    encrypt: true,
    trustServerCertificate: true,
  },
};

export async function startConnection() {
  try {
    const pool = await sql.connect(configDatabase);
    return pool;
  } catch (error) {
    console.error(error);
  }
}

export { sql };
