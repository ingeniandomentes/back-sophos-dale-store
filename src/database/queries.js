export const queries = {
  // Consultas de productos
  getAllProductos: "sp_getAllProductos",
  postNewProducto: "sp_postNewProducto @name ,@description, @url, @unit_price, @stock",
  getProductoById: "sp_getProductoById @id",
  putProductoById: "sp_putProductoById @id,@name ,@description, @url, @unit_price, @stock",
  deleteProductoById: "sp_deleteProductoById @id",
};
