import { startConnection, sql } from "../database";

import { queries } from "../database";

export const getProductos = async (req, res) => {
  try {
    const pool = await startConnection();
    const result = await pool.request().query(queries.getAllProductos);
    res.json(result.recordset);
  } catch (error) {
    res.status(500).json(error.message);
  }
};

export const createNewProducto = async (req, res) => {
  const { name ,description, url, unit_price, stock } = req.body;
  if (name == null || description == null ||  url == null ||  unit_price == null ||  stock == null) {
    return res
      .status(400)
      .json({ msg: "Ups, te faltan campos por diligenciar" });
  }
  try {
    const pool = await startConnection();
    await pool
      .request()
      .input("name", sql.VarChar(50), name)
      .input("description", sql.VarChar(50), description)
      .input("url", sql.VarChar(50), url)
      .input("unit_price", sql.Int, unit_price)
      .input("stock", sql.Int, stock)
      .query(queries.postNewProducto);
    res.status(201).json("Producto creado con exito!");
  } catch (error) {
    res.status(500).json(error.message);
  }
};

export const getProductoById = async (req, res) => {
  const { id } = req.params;
  const pool = await startConnection();
  const result = await pool
    .request()
    .input("id", id)
    .query(queries.getProductoById);
  res.json(result.recordset[0]);
};

export const putProductoById = async (req, res) => {
  const { id } = req.params;
  const { name ,description, url, unit_price, stock } = req.body;
  if (name == null || description == null ||  url == null ||  unit_price == null ||  stock == null) {
    return res
      .status(400)
      .json({ msg: "Ups, te faltan campos por diligenciar" });
  }
  try {
    const pool = await startConnection();
    const result = await pool
      .request()
      .input("id", id)
      .input("name", sql.VarChar(50), name)
      .input("description", sql.VarChar(50), description)
      .input("url", sql.VarChar(50), url)
      .input("unit_price", sql.Int, unit_price)
      .input("stock", sql.Int, stock)
      .query(queries.putProductoById);
    res.status(200).json("¡Producto actualizado con exito!");
  } catch (error) {
    res.status(500).json(error.message);
  }
};

export const deleteProductoById = async (req, res) => {
  const { id } = req.params;
  const pool = await startConnection();
  const result = await pool
    .request()
    .input("id", id)
    .query(queries.deleteProductoById);
  res.status(200).json("¡Producto eliminado con exito!");
};
