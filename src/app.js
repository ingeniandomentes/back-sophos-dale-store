import express from "express";
import config from "./config";
import cors from "cors";

import productosRoutes from "./routes/productos.routes";

const app = express();
app.set("port", config.port);

app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cors());

app.use(productosRoutes);

export default app;
