import { Router } from "express";

import {
  getProductos,
  createNewProducto,
  putProductoById,
  getProductoById,
  deleteProductoById,
} from "../controllers/productos.controller";

const router = Router();

router.get("/productos", getProductos);

router.post("/productos", createNewProducto);

router.get("/productos/:id", getProductoById);

router.put("/productos/:id", putProductoById);

router.delete("/productos/:id", deleteProductoById);

export default router;
